import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';

class ProgressDialogHandler
{
  ProgressDialog pr;
  ProgressDialogHandler(BuildContext context){
    pr=ProgressDialog(context, isDismissible: false);
  }

  showDialog(String msg)
  {
    pr.style(
      message: msg,
      borderRadius: 10.0,
      backgroundColor: Colors.white,
      progressWidget: CircularProgressIndicator(),
      elevation: 10.0,
      insetAnimCurve: Curves.easeInOut,
      progress: 0.0,
      maxProgress: 100.0,
      progressTextStyle: TextStyle(
        color: Colors.black,
        fontSize: 13.0,
        fontWeight: FontWeight.w400,
      ),
      messageTextStyle: TextStyle(
        color: Colors.black,
        fontSize: 19.0,
        fontWeight: FontWeight.w600,
      ),
    );

    return  pr.show();
  }

  get isShow
  {
    return this.pr.isShowing();
  }
  hideDialog()
  {
    return pr.hide();
  }


}