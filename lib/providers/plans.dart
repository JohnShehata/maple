import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:maple/constants.dart';
import 'package:maple/models/plan.dart';

import 'package:maple/utilities/auth.dart';
import 'package:http/http.dart' as http;
import 'package:maple/utilities/database_helper.dart';
import 'package:maple/utilities/factory.dart';

class PlansProvider with ChangeNotifier {
  final auth = Auth();
  final fact = Factory();

  DatabaseHelper db = DatabaseHelper();

  testService() async {
    return http.get(
      MyZoneController.testToken,
      headers: {
        "Accept": "application/json",
        "content-type": "application/json",
        "Authorization": "Bearer " + await auth.token,
      },
    );
  }

  testService2(dynamic params) async {
    return http.post(MyZoneController.testToken2,
        headers: {
          "Accept": "application/json",
          "content-type": "application/json",
          "Authorization": "Bearer " + await auth.token,
        },
        body: json.encode(params));
  }

  refreshActivePlans() async {
    var result = await auth.getEmployeeActivePlans();
    if (result.statusCode != 200) return null;
    final List plans = json.decode(result.body);
    await db.loadPlans(plans);
    print('loaded completed');
  }

  Future<List<Plan>> getEmployeeActivePlans() async {
    List<Plan> plansList = [];
    var employee = await auth.currentUser();
    List plans = await db.getActivePlans(employee.userId);
    //print(plans);
    if (plans == null) return null;

    plansList = plans.map((p) {
      return fact.toPlan(p);
    }).toList();
    return plansList;
  }

  Future<List<ScheduleEvent>> getScheduleEventsByDate(
      int planId, DateTime d) async {
    List<ScheduleEvent> eventsList = [];
    var employee = await auth.currentUser();
    List result = await db.getScheduleEventsByDate(employee.userId, planId, d);
    if (result == null) return null;
    eventsList = result.map((r) {
      return fact.toScheduleEvent(r);
    }).toList();
    return eventsList;
  }

  Future<ScheduleEvent> getScheduleEventsByeventId(int eventId) async {
    ScheduleEvent event;
    List result = await db.getScheduleEventsById(eventId);
    if (result == null) return null;
    event = result.map((r) {
      return fact.toScheduleEvent(r);
    }).first;
    return event;
  }

  getPresentationsByPlanId(int planId) async {
    List presentations = await db.getPresentationsByPlanId(planId);
    //print(presentations);
    return presentations.map((p) => fact.toPresentation(p)).toList();
    //return null;
  }

  getPresentationFilesByPresentationId(int presentationId) async {
    List files = await db.getPresentationFilesByPresentationId(presentationId);
    //print(presentations);
    return files.map((f) => fact.toPresentationFile(f)).toList();
    //return null;
  }

  destroyDatabase() async {
    return db.destroyDatabase();
  }

  startCall(int eventId) async {
    await db.startCall(eventId);
    notifyListeners();
  }

  terminateCall(int eventId) async {
    await db.terminateCall(eventId);
    notifyListeners();
  }
}
