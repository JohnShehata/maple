class ActivePlanTable {
  static String tableName = 'activePlans';

  static String planId = 'Id';
  static String planName = 'planName';
  static String startDate = 'startDate';
  static String endDate = 'endDate';
  static String creationDate = 'creationDate';
  static String createdBy = 'createdBy';
  static String employeeId = 'employeeId';
}

class ScheduleEventTable {
  static String tableName = 'ScheduleEvents';


  static String eventId = 'Id';
  static String startTime = 'StartTime';
  static String endTime = 'EndTime';
  static String callStartTime = 'CallStartTime';
  static String callEndTime = 'CallEndTime';
  static String doctorId = 'DoctorId';
  static String doctorName = 'DoctorName';
  static String doctorEmail = 'DoctorEmail';
  static String doctorMobile = 'DoctorMobile';
  static String specialityId = 'SpecialityId';
  static String specialityName = 'SpecialityName';
  static String categoryId = 'CategoryId';
  static String categoryName = 'CategoryName';
  static String doctorAddressId = 'DoctorAddressId';
  static String address = 'Address';
  static String brickId = 'BrickId';
  static String brickName = 'BrickName';
  static String isCall = 'IsCall';
  static String employeeId = 'EmployeeId';
  static String needSync = 'NeedSync';
  static String planId = 'planId';

}


class PresentationsTable
{
  static String tableName = 'Presentations';

  static String id = 'ID';
  static String presentationName = 'PresentationName';
  static String presentationDescription = 'PresentationDescription';
  static String productId = 'ProductId';
  static String productLineId = 'ProductLineId';
  static String planId = 'PlanId';
}

class PresentationFilesTable
{
  static String tableName = 'PresentationFiles';

  static String id = 'ID';
  static String fileName = 'FileName';
  static String fileSize = 'FileSize';
  static String productPresentationId = 'ProductPresentationId';
  static String img64 = 'Img64';



}