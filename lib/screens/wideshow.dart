import 'dart:math';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:maple/models/plan.dart';
import 'package:maple/providers/plans.dart';
import 'package:maple/utilities/imageutil.dart';
import 'package:maple/widgets/waitingcircle.dart';
import 'package:provider/provider.dart';

class WideShow extends StatefulWidget {
  static const routeName = '/wideshow';

  @override
  @override
  _WideShowState createState() => _WideShowState();
}

class _WideShowState extends State<WideShow> {
  List<PresentationFile> files;
  final imgUtil = ImageUtil();

  int fileIndex;

  List<String> slide1 = [
    'http://172.107.203.234/api/api/products/ReadFile?FolderName=4&FileName=Slide1.JPG',
    'http://172.107.203.234/api/api/products/ReadFile?FolderName=4&FileName=Slide2.JPG',
    'http://172.107.203.234/api/api/products/ReadFile?FolderName=4&FileName=Slide3.JPG',
    'http://172.107.203.234/api/api/products/ReadFile?FolderName=4&FileName=Slide4.JPG',
    'http://172.107.203.234/api/api/products/ReadFile?FolderName=4&FileName=Slide5.JPG',
    'http://172.107.203.234/api/api/products/ReadFile?FolderName=4&FileName=Slide6.JPG',
    'http://172.107.203.234/api/api/products/ReadFile?FolderName=4&FileName=Slide7.JPG',
    'http://172.107.203.234/api/api/products/ReadFile?FolderName=4&FileName=Slide8.JPG',
    'http://172.107.203.234/api/api/products/ReadFile?FolderName=4&FileName=Slide9.JPG'
  ];

  @override
  void dispose() {
    super.dispose();
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  }

  @override
  Widget build(BuildContext context) {
    final planProvider = Provider.of<PlansProvider>(context, listen: false);

    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.landscapeRight, DeviceOrientation.landscapeRight]);
    Size size = MediaQuery.of(context).size;

    final routeArgs =
        ModalRoute.of(context).settings.arguments as Map<String, dynamic>;
    var presentationId = routeArgs['presentationId'];
    var fileId = routeArgs['fileId'];

    Future<List<PresentationFile>> getPresentationFiles() async {
      await Future.delayed(Duration(milliseconds: 200));
      this.files = await planProvider
          .getPresentationFilesByPresentationId(presentationId);
      this.fileIndex = this.files.indexWhere((f) {
        return f.id == fileId;
      });

      return this.files;
    }

    return FutureBuilder(
        future: getPresentationFiles(),
        builder: (context, snap) {
          if (snap.connectionState == ConnectionState.done) {
            return CarouselSlider(
              items: this.files.map((e) {
                return GestureDetector(
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                          image: MemoryImage(
                            imgUtil.loadImage(e.img64),
                          ),
                          fit: BoxFit.cover),
                    ),
                  ),
                );
              }).toList(),
              options: CarouselOptions(
                initialPage: fileIndex,
                enableInfiniteScroll: true,
                viewportFraction: 0.8,
                aspectRatio: 16 / 9,
                enlargeCenterPage: true,
                autoPlay: false,
                height: size.height * 0.8,
                scrollDirection: Axis.horizontal,
              ),
            );
          } else if (snap.connectionState == ConnectionState.waiting) {
            return WaitingCirlce();
          } else
            return Text('');
        });
  }
}
