import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:maple/constants.dart';
import 'package:maple/models/ScheduleEvent.dart';
import 'package:maple/models/enums.dart';
import 'package:maple/models/plan.dart';
import 'package:maple/models/visit.dart';
import 'package:maple/providers/plans.dart';
import 'package:maple/providers/progressdialoghandler.dart';
import 'package:maple/widgets/callstatuscount.dart';
import 'package:maple/widgets/draglines.dart';
import 'package:maple/widgets/schedulevisit.dart';
import 'package:maple/widgets/visitstats.dart';
import 'package:maple/widgets/waitingcircle.dart';
import 'package:provider/provider.dart';
import 'package:table_calendar/table_calendar.dart';

class MySchedule extends StatefulWidget {
  static const routeName = '/myschedule';

  @override
  _MyScheduleState createState() => _MyScheduleState();
}

class _MyScheduleState extends State<MySchedule> {
  CalendarController _controller;
  List<ScheduleEvent> eventsList;
  ProgressDialogHandler progressHandler;
  DateTime lastSelectedDate;

  TextStyle dayStyle(FontWeight fontWeight) {
    return TextStyle(color: Color(0xff30384c), fontWeight: fontWeight);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _controller = CalendarController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // SystemChrome.setPreferredOrientations(
    //     [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
    final planProvider = Provider.of<PlansProvider>(context, listen: true);

    //print('called from Schedules' + DateTime.now().toString());
    progressHandler = ProgressDialogHandler(context);

    final routeArgs =
        ModalRoute.of(context).settings.arguments as Map<String, dynamic>;
    var startDate = routeArgs['startDate'];
    var endDate = routeArgs['endDate'];
    int planId = routeArgs['planId'];
    Size size = MediaQuery.of(context).size;
    var doneCalls = 0, inProgressCall = 0, unFinishedCalls = 0;

    calculateCalls() async {
      await Future.delayed(Duration(milliseconds: 500));
      if (lastSelectedDate == null)
        lastSelectedDate = DateTime.parse(startDate.toString());

      eventsList =
          await planProvider.getScheduleEventsByDate(planId, lastSelectedDate);

      if (this.eventsList == null || this.eventsList.length == 0) return null;
      this.eventsList.forEach((call) {
        if (call.isCall == 1)
          doneCalls += 1;
        else if (call.callStartTime != null && call.callEndTime == null)
          inProgressCall += 1;
        else
          unFinishedCalls += 1;
      });
      return eventsList;
    }

    //await calculateCalls();
    //print('doneCalls' + doneCalls.toString());

    return Scaffold(
      backgroundColor: kPrimaryColor,
      body: FutureBuilder(
        future: calculateCalls(),
        builder: (context, snap) {
          if (snap.connectionState == ConnectionState.done) {
            return Stack(
              children: [
                Column(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    TableCalendar(
                      startDay: DateTime.parse(startDate),
                      endDay: DateTime.parse(endDate),
                      initialSelectedDay: lastSelectedDate,
                      startingDayOfWeek: StartingDayOfWeek.monday,
                      onCalendarCreated: (d1, d2, _) async {},
                      onDaySelected: (d1, x1, x2) async {
                        setState(() {
                          lastSelectedDate = d1;
                        });
                      },
                      calendarStyle: CalendarStyle(
                        weekdayStyle: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: Colors.white,
                        ),
                        weekendStyle: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: Colors.red,
                        ),
                        selectedColor: Colors.grey,
                        todayColor: kPrimaryColor,
                      ),
                      daysOfWeekStyle: DaysOfWeekStyle(
                        weekdayStyle: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                        ),
                        weekendStyle: TextStyle(
                          color: Colors.red,
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                        ),
                      ),
                      headerStyle: HeaderStyle(
                        formatButtonVisible: false,
                        titleTextStyle: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ),
                      calendarController: _controller,
                    ),
                    (this.eventsList == null || this.eventsList.length == 0
                        ? Column(
                            children: [
                              Center(
                                child: Image.asset(
                                  'assets/images/calendar.png',
                                  height: size.height * 0.2,
                                ),
                              ),
                              Text(
                                'No Visits For The Selected Date',
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          )
                        : Text(''))
                  ],
                ),
                (this.eventsList != null && this.eventsList.length > 0
                    ? DraggableScrollableSheet(
                        initialChildSize: 0.2,
                        minChildSize: 0.2,
                        maxChildSize: 0.8,
                        builder: (context, controller) {
                          return Container(
                            decoration: BoxDecoration(
                              color: kPrimaryLightColor,
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10),
                                topRight: Radius.circular(10),
                              ),
                            ),
                            child: ListView.builder(
                              controller: controller,
                              itemCount: this.eventsList.length,
                              itemBuilder: (BuildContext context, index) {
                                return (index == 0
                                    ? Column(
                                        children: [
                                          DragLines(),
                                          SizedBox(
                                            height: 10,
                                          ),
                                          VisitState(
                                            doneCalls: doneCalls,
                                            unfinishedCalls: unFinishedCalls,
                                            inprogressCalls: inProgressCall,
                                          ),
                                          // DragLines(),
                                          SizedBox(
                                            height: 10,
                                          ),
                                          ScheduleVisit(this.eventsList[index],
                                              CardVisitTypeEnum.Visit)
                                        ],
                                      )
                                    : ScheduleVisit(this.eventsList[index],
                                        CardVisitTypeEnum.Visit));
                              },
                            ),
                          );
                        },
                      )
                    : Text(''))
              ],
            );
          } else if (snap.connectionState == ConnectionState.waiting) {
            return WaitingCirlce();
          } else
            return Container(
              color: kPrimaryColor,
            );
        },
      ),
    );
  }
}
