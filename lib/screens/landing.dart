import 'package:flutter/material.dart';
import 'package:maple/utilities/auth.dart';
import 'package:maple/screens/activeplans.dart';
import 'package:maple/screens/login.dart';

import '../constants.dart';

class LandingScreen extends StatelessWidget {
  final auth = new Auth();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    auth.isLoggined().then(
      (value) {
        // print(DateTime.now().toString());
        if (value == true) {
          Navigator.of(context).pushReplacementNamed(ActivePlans.routeName);
        } else {
          Navigator.of(context).pushReplacementNamed(LoginScreen.routeName);
        }
      },
    );

    return Scaffold(
      backgroundColor: kPrimaryColor,
      body: Center(
        child: Image.asset(
          'assets/images/landingimage.png',
          height: size.height * 0.4,
        ),
      ),
    );
  }
}
