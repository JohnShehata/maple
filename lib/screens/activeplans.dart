import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:maple/constants.dart';
import 'package:maple/models/plan.dart';
import 'package:maple/providers/plans.dart';
import 'package:maple/providers/progressdialoghandler.dart';
import 'package:maple/screens/myschedule.dart';
import 'package:maple/screens/test.dart';
import 'package:maple/utilities/database_helper.dart';
import 'package:maple/widgets/mydrawer.dart';
import 'package:maple/widgets/plancard.dart';
import 'package:maple/widgets/waitingcircle.dart';
import 'package:provider/provider.dart';

class ActivePlans extends StatefulWidget {
  static const routeName = '/actipvelans';

  @override
  _ActivePlansState createState() => _ActivePlansState();
}

class _ActivePlansState extends State<ActivePlans> {
  DatabaseHelper db = DatabaseHelper();
  ProgressDialogHandler progressHandler;
  List<Plan> plansList;

  @override
  void initState() {
    print('first time');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // SystemChrome.setPreferredOrientations(
    //     [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
    Size size = MediaQuery.of(context).size;
    final planProvider = Provider.of<PlansProvider>(context, listen: true);
    progressHandler = ProgressDialogHandler(context);

    Future<List<Plan>> getActivePlans() async {
      plansList = await planProvider.getEmployeeActivePlans();
      return plansList;
    }

    return Scaffold(
      backgroundColor: kPrimaryColor,
      appBar: AppBar(
        backgroundColor: kPrimaryColor,
        centerTitle: true,
        actions: [
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: () async {
              await planProvider.refreshActivePlans();
              await getActivePlans();
              setState(() {});
            },
          )
        ],
        title: Text(
          'Active Plans',
          textAlign: TextAlign.center,
        ),
      ),
      drawer: MyDrawer(),
      body: FutureBuilder(
        future: getActivePlans(),
        builder: (context, snap) {
          if (snap.connectionState == ConnectionState.done) {
            return Container(
              padding: EdgeInsets.all(6),
              color: kPrimaryColor,
              height: double.infinity,
              child: (plansList != null && plansList.length > 0
                  ? SingleChildScrollView(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            ...plansList.map((pL) {
                              return InkWell(
                                onTap: () {
                                  Navigator.of(context).pushNamed(
                                    MySchedule.routeName,
                                    //TestLoading.routeName,
                                    arguments: {
                                      'planId': pL.id,
                                      'startDate': pL.startDate.toString(),
                                      'endDate': pL.endDate.toString(),
                                    },
                                  );
                                },
                                child: PlanCard(pL),
                              );
                            }).toList(),
                            // RaisedButton(
                            //   autofocus: false,
                            //   clipBehavior: Clip.none,
                            //   onPressed: () async {
                            //     await planProvider.refreshActivePlans();
                            //     setState(() {});
                            //   },
                            //   child: Text('REFRESH'),
                            // ),
                            // RaisedButton(
                            //   autofocus: false,
                            //   clipBehavior: Clip.none,
                            //   onPressed: () async {
                            //     var d = DateTime(2021, 3, 20);

                            //     var result =
                            //         await planProvider.getScheduleEventsByDate(
                            //       8,
                            //       d,
                            //     );
                            //     setState(() {});
                            //   },
                            //   child: Text('GET SCHEDULE EVENTS'),
                            // ),
                            // RaisedButton(
                            //   onPressed: () async {
                            //     var plans =
                            //         await planProvider.getEmployeeActivePlans();
                            //     setState(() {});
                            //   },
                            //   child: Text('Read for SQL'),
                            // ),
                            TextButton(
                              onPressed: () async {
                                await progressHandler
                                    .showDialog('Clear tables');
                                await db.resetData();
                                await progressHandler.hideDialog();
                                setState(() {});
                              },
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.delete,
                                    color: Colors.red,
                                  ),
                                  // Text(
                                  //   'Clear Tables',
                                  //   style: TextStyle(
                                  //     color: Colors.white,
                                  //   ),
                                  // )
                                ],
                              ),
                            ),
                            // RaisedButton(
                            //   onPressed: () async {
                            //     await db.destroyDatabase();
                            //     setState(() {});
                            //     print('Destroyed successfully');
                            //   },
                            //   child: Text('Destroy Database'),
                            // )
                          ]
                          // children: [
                          //   InkWell(
                          //     onTap: () {
                          //       Navigator.of(context).pushNamed(MySchedule.routeName);
                          //     },
                          //     child: PlanCard(),
                          //   ),
                          //   RaisedButton(
                          //     onPressed: () async {
                          //       await planProvider.refreshActivePlans();
                          //     },
                          //     child: Text('REFRESH'),
                          //   ),
                          //   RaisedButton(
                          //     onPressed: () async {
                          //      var plans= await planProvider.getEmployeeActivePlans();
                          //     },
                          //     child: Text('Read for SQL'),
                          //   ),
                          //   RaisedButton(
                          //     onPressed: () async {
                          //       await db.resetData();
                          //     },
                          //     child: Text('Clear Tables'),
                          //   ),
                          //   RaisedButton(
                          //     onPressed: () async {
                          //       await db.destroyDatabase();
                          //       print('Destroyed successfully');
                          //     },
                          //     child: Text('Destroy Database'),
                          //   )
                          // ],
                          ),
                    )
                  : Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Center(
                          child: Image.asset(
                            'assets/images/noplans.png',
                            height: size.height * 0.4,
                          ),
                        ),
                        Text(
                          'No Active Plans Found !',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                        // OutlineButton(
                        //   onPressed: () async {
                        //     await progressHandler
                        //         .showDialog('Getiing Active Plans');
                        //     //await planProvider.destroyDatabase();
                        //     await planProvider.refreshActivePlans();
                        //     await getActivePlans();
                        //     await progressHandler.hideDialog();
                        //     setState(() {});
                        //   },
                        //   child: Text('Load Active Plans'),
                        // )
                      ],
                    )),
            );
          } else if (snap.connectionState == ConnectionState.waiting) {
            return WaitingCirlce();
          } else
            return Container(
              color: kPrimaryColor,
            );
        },
      ),
    );
  }
}
