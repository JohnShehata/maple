import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:maple/models/VisitNote.dart';
import 'package:maple/models/enums.dart';
import 'package:maple/models/plan.dart';
import 'package:maple/models/visit.dart';
import 'package:maple/providers/plans.dart';
import 'package:maple/widgets/draglines.dart';
import 'package:maple/widgets/schedulevisit.dart';
import 'package:maple/widgets/slideshow.dart';
import 'package:maple/widgets/visitnotecard.dart';
import 'package:maple/widgets/waitingcircle.dart';
import 'package:provider/provider.dart';

import '../constants.dart';

class VisitDetails extends StatefulWidget {
  static const routeName = '/visitdetails';

  @override
  _VisitDetailsState createState() => _VisitDetailsState();
}

class _VisitDetailsState extends State<VisitDetails> {
  ScheduleEvent scheduleEvent;
  List<Presentation> presentations;

  // List<String> slide1 = [
  //   'http://172.107.203.234/api/api/products/ReadFile?FolderName=4&FileName=Slide1.JPG',
  //   'http://172.107.203.234/api/api/products/ReadFile?FolderName=4&FileName=Slide2.JPG',
  //   'http://172.107.203.234/api/api/products/ReadFile?FolderName=4&FileName=Slide3.JPG',
  //   'http://172.107.203.234/api/api/products/ReadFile?FolderName=4&FileName=Slide4.JPG',
  //   'http://172.107.203.234/api/api/products/ReadFile?FolderName=4&FileName=Slide5.JPG',
  //   'http://172.107.203.234/api/api/products/ReadFile?FolderName=4&FileName=Slide6.JPG',
  //   'http://172.107.203.234/api/api/products/ReadFile?FolderName=4&FileName=Slide7.JPG',
  //   'http://172.107.203.234/api/api/products/ReadFile?FolderName=4&FileName=Slide8.JPG',
  //   'http://172.107.203.234/api/api/products/ReadFile?FolderName=4&FileName=Slide9.JPG'
  // ];

  // List<String> slide2 = [
  //   'http://172.107.203.234/api/api/products/ReadFile?FolderName=5&FileName=Slide1.JPG',
  //   'http://172.107.203.234/api/api/products/ReadFile?FolderName=5&FileName=Slide2.JPG',
  //   'http://172.107.203.234/api/api/products/ReadFile?FolderName=5&FileName=Slide3.JPG',
  //   'http://172.107.203.234/api/api/products/ReadFile?FolderName=5&FileName=Slide4.JPG',
  //   'http://172.107.203.234/api/api/products/ReadFile?FolderName=5&FileName=Slide5.JPG',
  //   'http://172.107.203.234/api/api/products/ReadFile?FolderName=5&FileName=Slide6.JPG',
  // ];

  List<VisitNote> notes = [
    VisitNote(
      Note:
          'ndustry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in',
      NoteTime: 'Feb 22, 2021 11:34 am',
    ),
    VisitNote(
      Note:
          'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which dont look even slightly believable. If you are going to use a passage of Lorem Ipsum',
      NoteTime: 'Mar 10, 2021 09:05 am',
    )
  ];

  Visit currentVisit = Visit(
    doctorName: 'Isam Aqeel Haytham Thaqib',
    brickName: 'BRICK2 DAKAHLEIA II',
    cardType: CardVisitTypeEnum.Details,
    category: 'class c',
    email: 'IsamAqeelHaythamThaqib@email.com',
    location: 'horria street , maadi',
    phoneNumber: '222-985-92929',
    speciality: 'Pulmonologists',
    visitStatus: VisitStatusEnum.NotVisited,
    appointmentTime: 'Feb 11 , 2021 05:14 pm',
  );

  @override
  Widget build(BuildContext context) {
    // SystemChrome.setPreferredOrientations(
    //     [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
    Size size = MediaQuery.of(context).size;
    final planProvider = Provider.of<PlansProvider>(context);
    final routeArgs =
        ModalRoute.of(context).settings.arguments as Map<String, dynamic>;
    final int eventId = routeArgs['eventId'];
    final int planId = routeArgs['planId'];

    Future<ScheduleEvent> getScheduleEventsByeventId() async {
      await Future.delayed(Duration(milliseconds: 200));
      this.presentations = await planProvider.getPresentationsByPlanId(planId);

      // print('presentations');
      // print(presentations);

      this.scheduleEvent =
          await planProvider.getScheduleEventsByeventId(eventId);
      return this.scheduleEvent;
    }

    return Scaffold(
      backgroundColor: kPrimaryColor,
      body: FutureBuilder(
          future: getScheduleEventsByeventId(),
          builder: (context, snap) {
            if (snap.connectionState == ConnectionState.done) {
              return Stack(
                children: [
                  SingleChildScrollView(
                    child: Column(
                      children: [
                        SizedBox(
                          height: 40,
                        ),
                        Visibility(
                          visible: (this.scheduleEvent.callStartTime == null &&
                              this.scheduleEvent.callEndTime == null &&
                              this.scheduleEvent.isCall != 1),
                          child: Container(
                            width: size.width * 0.9,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(10),
                              child: TextButton(
                                style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          Colors.green[700]),
                                ),
                                onPressed: () async {
                                  await planProvider
                                      .startCall(this.scheduleEvent.eventId);
                                  this.scheduleEvent.callStartTime =
                                      DateTime.now().toString();
                                  setState(() {});
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(
                                      Icons.play_arrow,
                                      color: Colors.white,
                                    ),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    Text(
                                      'Start',
                                      style: TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                        Visibility(
                          visible: this.scheduleEvent.callStartTime != null &&
                              this.scheduleEvent.callEndTime == null &&
                              this.scheduleEvent.isCall != 1,
                          child: Container(
                            width: size.width * 0.9,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(10),
                              child: TextButton(
                                style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          Colors.red),
                                ),
                                onPressed: () async {
                                  await planProvider.terminateCall(
                                      this.scheduleEvent.eventId);
                                  this.scheduleEvent.callEndTime =
                                      DateTime.now().toString();
                                  setState(() {});
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(
                                      Icons.stop,
                                      color: Colors.white,
                                    ),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    Text(
                                      'Terminate',
                                      style: TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 5),
                        ScheduleVisit(
                            this.scheduleEvent, CardVisitTypeEnum.Details),
                        SizedBox(height: 5),
                        Container(
                          width: size.width * 0.9,
                          decoration: BoxDecoration(
                              border: Border(
                            top: BorderSide(
                                width: 1.0, color: kPrimaryLightColor),
                          )),
                        ),
                        SizedBox(height: 30),
                        ...presentations.map((p) {
                          return Column(
                            children: [
                              SlideShow(
                                presentation: p,
                              ),
                              SizedBox(height: 30),
                            ],
                          );
                        }).toList(),
                        SizedBox(height: 60),
                      ],
                    ),
                  ),
                  DraggableScrollableSheet(
                    initialChildSize: 0.1,
                    minChildSize: 0.1,
                    maxChildSize: 0.8,
                    builder: (context, controller) {
                      return Container(
                        decoration: BoxDecoration(
                          color: kPrimaryLightColor,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10),
                          ),
                        ),
                        child: ListView.builder(
                          controller: controller,
                          itemCount: this.notes.length,
                          itemBuilder: (BuildContext context, index) {
                            return (index == 0
                                ? Column(
                                    children: [
                                      DragLines(),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      VisitNoteCard(this.notes[index])
                                    ],
                                  )
                                : (index == this.notes.length - 1
                                    ? Column(
                                        children: [
                                          VisitNoteCard(this.notes[index]),
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: TextField(
                                              maxLines: 5,
                                              decoration: InputDecoration(
                                                filled: true,
                                                fillColor: Colors.white,
                                              ),
                                            ),
                                          )
                                        ],
                                      )
                                    : VisitNoteCard(this.notes[index])));
                          },
                        ),
                      );
                    },
                  )
                ],
              );
            } else if (snap.connectionState == ConnectionState.waiting) {
              return WaitingCirlce();
            } else
              return Text('');
          }),
    );
  }
}
