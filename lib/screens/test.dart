import 'dart:io';

import 'package:flutter/material.dart';
import 'package:maple/constants.dart';
import 'package:maple/utilities/imageutil.dart';

class TestLoading extends StatefulWidget {
  static const routeName = '/TestLoading';

  @override
  _TestLoadingState createState() => _TestLoadingState();
}

class _TestLoadingState extends State<TestLoading> {
  final imgUtil = ImageUtil();
  var imageMemory;
  String img64;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        backgroundColor: kPrimaryColor,
        appBar: AppBar(
          title: Text('Test'),
        ),
        body: Column(
          children: [
            TextButton(
              onPressed: () async {
                this.img64 = await imgUtil.downloadImage(
                    'http://172.107.203.234/api/api/products/ReadFile?FolderName=2&FileName=Slide15.JPG');
                print('downloaded');
              },
              child: Text('Download Image'),
            ),
            TextButton(
              onPressed: () async {
                var file = imgUtil.loadImage(img64);
                setState(() {
                  this.imageMemory = file;
                });
              },
              child: Text('Load Image'),
            ),
            Container(
              child: (this.imageMemory != null
                  ? Image.memory(this.imageMemory)
                  : Text('')),
            )
          ],
        ));
  }
}
