// import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:maple/providers/plans.dart';
// import 'package:flutter/services.dart';
// import 'package:flutter_svg/flutter_svg.dart';
// import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:maple/utilities/auth.dart';
import 'package:maple/providers/progressdialoghandler.dart';
import 'package:maple/screens/activeplans.dart';
import 'package:maple/widgets/emailtextfield.dart';
import 'package:maple/widgets/passwordtextfield.dart';
import 'package:maple/widgets/roundedbutton.dart';
import 'package:provider/provider.dart';
// import 'package:provider/provider.dart';
import '../constants.dart';

class LoginScreen extends StatefulWidget {
  static const routeName = '/login';
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  Auth auth = Auth();

  ProgressDialogHandler progressHandler;

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  String email;
  String password;

  bool iconon = false;

  toggleIcon() {
    setState(() {
      this.iconon = !this.iconon;
    });
  }

  showFailedDialog() {
    showDialog(
      context: context,
      builder: (_) => AlertDialog(
        content: Text('Failed to login'),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text('Ok'),
          )
        ],
      ),
      barrierDismissible: true,
    );
  }

  @override
  Widget build(BuildContext context) {
    final planProvider = Provider.of<PlansProvider>(context);
    progressHandler = ProgressDialogHandler(context);
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      body: Container(
        color: kPrimaryColor,
        width: double.infinity,
        height: size.height,
        child: Stack(
          alignment: Alignment.center,
          children: [
            SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      'assets/images/doctors.png',
                      height: size.height * 0.4,
                    ),
                    EmailTextField(
                      onSave: (String value) {
                        email = value;
                      },
                    ),
                    PasswordTextField(
                      onSave: (String value) {
                        password = value;
                      },
                    ),
                    RoundedButton(
                      buttonLbl: 'Login',
                      onPress: () async {
                        if (!_formKey.currentState.validate()) return null;
                        _formKey.currentState.save();
                        try {
                          await this
                              .progressHandler
                              .showDialog('Authorization');
                          auth
                              .login(
                            email: email,
                            password: password,
                          )
                              .then((response) async {
                            await progressHandler.hideDialog();
                            var result = json.decode(response.body);
                            var token = result['token'];
                            if (response.statusCode == 200 && token != null) {
                              auth.setToken(token);

                            // final params={'EmployeeId':'JoJo5'};
                            // var test=await planProvider.testService2(params);
                            // print(test.statusCode);
                            // print(test.body);

                              Navigator.of(context)
                                  .pushReplacementNamed(ActivePlans.routeName);

                            } else {
                              showFailedDialog();
                            }
                          }).catchError((err) async {
                            print(err);
                            await this.progressHandler.hideDialog();
                            showFailedDialog();
                          });
                        } catch (e) {
                          await this.progressHandler.hideDialog();
                          showFailedDialog();
                        }
                      },
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
