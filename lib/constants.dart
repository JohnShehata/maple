import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xff30384c);
const kPrimaryLightColor = Color(0xFF3F4B69);

const _apiUrl = 'http://172.107.203.234/api/api';
//const _apiUrl='http://10.0.2.2:50213/api';

class AuthController
{
  static const String _controllerSegment='/employees/';
  static const String login=_apiUrl+_controllerSegment+'login';
  static const String test=_apiUrl+_controllerSegment+'test';

}

class MyZoneController
{
  static const String _controllerSegment='/MyZone/';

  static const String testToken=_apiUrl+_controllerSegment+'TestToken';
  static const String testToken2=_apiUrl+_controllerSegment+'TestToken2';
  static const String getEmployeeActivePlans =_apiUrl+_controllerSegment+'GetEmployeeActivePlans';

}

class ProductsController
{
  static const String _productSegment='/products/';

  static const String readFile =_apiUrl+_productSegment+'ReadFile';

}


