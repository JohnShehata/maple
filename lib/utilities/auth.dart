import 'dart:convert';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:maple/constants.dart';
import 'package:maple/models/user.dart';

class Auth {
  final _storage = FlutterSecureStorage();
  final String _tokenkey = 'token';

  Future<dynamic> login({String email, String password}) {
    return http.post(
      AuthController.login,
      headers: {
        "Accept": "application/json",
        "content-type": "application/json",
      },
      body: json.encode({'Email': email, 'Password': password}),
    );
  }

  Future<void> setToken(String token) async {
    return await _storage.write(key: _tokenkey, value: token);
  }

  Future<bool> isLoggined() async {
    final String tokenvalue = await _storage.read(key: _tokenkey);
    if (tokenvalue == null || tokenvalue == '') return false;
    return true;
  }

  Future<User> currentUser() async {
    if (!await this.isLoggined()) return null;
    var token = await _storage.read(key: _tokenkey);
    dynamic decodedToken = JwtDecoder.decode(token);
    return User(
      name: decodedToken['unique_name'],
      email: decodedToken['email'],
      userId:int.parse(decodedToken['nameid']) ,
    );
  }

  Future<String> get token async {
    if (!await this.isLoggined()) return null;
    return await _storage.read(key: _tokenkey);
  }

  Future<void> logout() async {
    return await _storage.delete(key: _tokenkey);
  }

  Future<dynamic> _post(String url, Map<String, dynamic> params) async {
    return http.post(
      url,
      headers: {
        "Accept": "application/json",
        "content-type": "application/json",
        "Authorization": "Bearer " + await this.token
      },
      body: json.encode(params),
    );
  }

  Future<dynamic> _get(String url, Map<String, dynamic> params) async {
    var endpointUrl = url;

    Map<String, String> headers = {
      "Accept": "application/json",
      "content-type": "application/json",
      "Authorization": "Bearer " + await this.token
    };
    String queryString = Uri(queryParameters: params).query;
    var requestUrl = endpointUrl + '?' + queryString;
    return http.get(
      requestUrl,
      headers: headers,
    );
  }


  getEmployeeActivePlans() async {
    return http.get(
      MyZoneController.getEmployeeActivePlans,
      headers: {
        "Accept": "application/json",
        "content-type": "application/json",
        "Authorization": "Bearer " + await this.token,
      },
    );
  }
}
