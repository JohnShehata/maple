import 'dart:io';

import 'package:maple/models/plan.dart';
import 'package:maple/providers/tables.dart';
import 'package:maple/utilities/auth.dart';
import 'package:maple/utilities/imageutil.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

import '../constants.dart';

class DatabaseHelper {
  var auth = Auth();
  var util = ImageUtil();

  static DatabaseHelper _databaseHelper;
  static Database _database;

  String databaseName = 'userPlans.db';

  DatabaseHelper._createInstance();
  factory DatabaseHelper() {
    if (_databaseHelper == null)
      _databaseHelper = DatabaseHelper._createInstance();

    return _databaseHelper;
  }

  Future<Database> get database async {
    if (_database == null) {
      _database = await initializeDatabase();
    }
    return _database;
  }

  Future<Database> initializeDatabase() async {
    print('Initialize DB');
    Directory directory = await getApplicationDocumentsDirectory();
    String path = directory.path + databaseName;
    var dbRef = await openDatabase(path, version: 1, onCreate: _createDB);
    return dbRef;
  }

  void _createDB(Database db, int newVersion) async {
    var createActivePlansTableQuery = '''
    CREATE table
    ${ActivePlanTable.tableName}
    (
      ${ActivePlanTable.planId} INTEGER PRIMARY KEY ,
      ${ActivePlanTable.planName} TEXT,
      ${ActivePlanTable.startDate} TEXT,
      ${ActivePlanTable.endDate} TEXT,
      ${ActivePlanTable.createdBy} TEXT,
      ${ActivePlanTable.employeeId} INTEGER
    )
    ''';
    //***** */
    var createScheduleEventsTableQuery = '''
    CREATE table
    ${ScheduleEventTable.tableName}
    (
      ${ScheduleEventTable.eventId} INTEGER PRIMARY KEY ,
      ${ScheduleEventTable.startTime} TEXT,
      ${ScheduleEventTable.endTime} TEXT,
      ${ScheduleEventTable.callStartTime} TEXT,
      ${ScheduleEventTable.callEndTime} TEXT,
      ${ScheduleEventTable.doctorId} INTEGER,
      ${ScheduleEventTable.doctorName} TEXT,
      ${ScheduleEventTable.doctorEmail} TEXT,
      ${ScheduleEventTable.doctorMobile} TEXT,
      ${ScheduleEventTable.specialityId} INTEGER,
      ${ScheduleEventTable.specialityName} TEXT,
      ${ScheduleEventTable.categoryId} INTEGER,
      ${ScheduleEventTable.categoryName} TEXT,
      ${ScheduleEventTable.doctorAddressId} INTEGER,
      ${ScheduleEventTable.address} TEXT,
      ${ScheduleEventTable.brickId} INTEGER,
      ${ScheduleEventTable.brickName} TEXT,
      ${ScheduleEventTable.planId} INTEGER,
      ${ScheduleEventTable.isCall} INTEGER,
      ${ScheduleEventTable.employeeId} INTEGER,
      ${ScheduleEventTable.needSync} INTEGER
    )
    ''';

    var createPresentationsTableQuery = '''
    CREATE table
    ${PresentationsTable.tableName}
    (
      ${PresentationsTable.id} INTEGER PRIMARY KEY ,
      ${PresentationsTable.presentationName} TEXT,
      ${PresentationsTable.presentationDescription} TEXT,
      ${PresentationsTable.productId} INTEGER,
      ${PresentationsTable.productLineId} INTEGER,
      ${PresentationsTable.planId} INTEGER
    )
    ''';

    var createPresentationFilesTableQuery = '''
    CREATE table
    ${PresentationFilesTable.tableName}
    (
      ${PresentationFilesTable.id} INTEGER PRIMARY KEY ,
      ${PresentationFilesTable.fileName} TEXT,
      ${PresentationFilesTable.productPresentationId} INTEGER,
      ${PresentationFilesTable.img64} TEXT
    )
    ''';

    await db.execute(createActivePlansTableQuery);
    await db.execute(createScheduleEventsTableQuery);
    await db.execute(createPresentationsTableQuery);
    await db.execute(createPresentationFilesTableQuery);
  }

  loadPlans(List plans) async {
    plans.forEach((element) async {
      await insertPlan(element);
      var events = element["ScheduleEvents"];
      var presentations = element["Presentations"];

      events.forEach((event) async {
        await insertScheduleEvent(event);
      });

      if (presentations != null) {
        presentations.forEach((presentation) async {
          //print(presentation);
          await insertPresentation(presentation);
          var files = presentation["Files"];
          if (files != null) {
            files.forEach((file) async {
              await insertPresentationFiles(file);
            });
          }
        });
      }
    });
  }

  insertPlan(dynamic plan) async {
    var db = await this.database;
    var user = await auth.currentUser();
    var count = await db.rawQuery(
        "select count(*) from ${ActivePlanTable.tableName} where ${ActivePlanTable.planId}=${plan['Id']}");
    var existRows = count[0]['count(*)'];
    if (existRows != null && existRows == 0) {
      var query = '''
          insert into ${ActivePlanTable.tableName}
          (
            ${ActivePlanTable.planId},
            ${ActivePlanTable.planName},
            ${ActivePlanTable.startDate},
            ${ActivePlanTable.endDate},
            ${ActivePlanTable.createdBy},
            ${ActivePlanTable.employeeId}
          )
            VALUES
          (
            ${plan["ID"]},
            ${concat(plan["PlaneName"])},
            ${concat(plan["StartDate"])},
            ${concat(plan["EndDate"])},
            ${concat(plan["CreatedByName"])},
             ${user.userId}
          )
          ''';
      return await db.rawInsert(query);
    }
  }

  insertScheduleEvent(dynamic schedule) async {
    var db = await this.database;
    var count = await db.rawQuery(
        "select count(*) from ${ScheduleEventTable.tableName} where ${ScheduleEventTable.eventId}=${schedule['ID']}");
    var existRows = count[0]['count(*)'];
    if (existRows != null && existRows == 0) {
      var query = '''
          insert into ${ScheduleEventTable.tableName}
          (
            ${ScheduleEventTable.eventId} ,
            ${ScheduleEventTable.startTime} ,
            ${ScheduleEventTable.endTime} ,
            ${ScheduleEventTable.callStartTime},
            ${ScheduleEventTable.callEndTime} ,
            ${ScheduleEventTable.doctorId} ,
            ${ScheduleEventTable.doctorName} ,
            ${ScheduleEventTable.doctorEmail} ,
            ${ScheduleEventTable.doctorMobile} ,
            ${ScheduleEventTable.specialityId} ,
            ${ScheduleEventTable.specialityName} ,
            ${ScheduleEventTable.categoryId} ,
            ${ScheduleEventTable.categoryName} ,
            ${ScheduleEventTable.doctorAddressId} ,
            ${ScheduleEventTable.address} ,
            ${ScheduleEventTable.brickId} ,
            ${ScheduleEventTable.brickName} ,
            ${ScheduleEventTable.planId} ,
            ${ScheduleEventTable.isCall} ,
            ${ScheduleEventTable.employeeId} ,
            ${ScheduleEventTable.needSync}
          )
            VALUES
          (
            ${schedule["ID"]},
            ${concat(schedule["StartTime"])},
            ${concat(schedule["EndTime"])},
            ${concat(schedule["CallStartTime"])},
            ${concat(schedule["CallEndTime"])},
            ${schedule["DoctorId"]},
            ${concat(schedule["DoctorName"])},
            ${concat(schedule["DoctorEmail"])},
            ${concat(schedule["DoctorMobile"])},
            ${schedule["SpecialityId"]},
            ${concat(schedule["SpecialityName"])},
            ${schedule["CategoryId"]},
            ${concat(schedule["CategoryName"])},
            ${schedule["doctorAddressId"]},
            ${concat(schedule["Address"])},
            ${schedule["BrickId"]},
            ${concat(schedule["BrickName"])},
            ${schedule["PlanId"]},
            ${schedule["IsCall"]},
            ${schedule["EmployeeId"]},
            0
          )
          ''';
      return await db.rawInsert(query);
    }
  }

  insertPresentation(dynamic presentation) async {
    var db = await this.database;
    var query = '''
      insert into ${PresentationsTable.tableName}
      (
        ${PresentationsTable.id},
        ${PresentationsTable.presentationName},
        ${PresentationsTable.presentationDescription},
        ${PresentationsTable.productId},
        ${PresentationsTable.productLineId},
        ${PresentationsTable.planId}
      )
      VALUES
      (
        ${presentation["ID"]},
        ${concat(presentation["PresentationName"])},
        ${concat(presentation["PresentationDescription"])},
        ${presentation["ProductId"]},
        ${presentation["ProductLineId"]},
        ${presentation["PlanId"]}
      )
    ''';
    return await db.rawInsert(query);
  }

  insertPresentationFiles(dynamic file) async {
    var db = await this.database;

    var params = {
      'FolderName': file["ProductPresentationId"].toString(),
      'FileName': file["FileName"]
    };

    String endpointUrl = ProductsController.readFile;
    String queryString = Uri(queryParameters: params).query;
    var requestUrl = endpointUrl + '?' + queryString;
    //print(requestUrl);
    var img64 = await util.downloadImage(requestUrl);
    //print(img64);
    var query = '''
      insert into ${PresentationFilesTable.tableName}
      (
        ${PresentationFilesTable.id},
        ${PresentationFilesTable.fileName},
        ${PresentationFilesTable.productPresentationId},
        ${PresentationFilesTable.img64}
      )
      VALUES
      (
        ${file["ID"]},
        ${concat(file["FileName"])},
        ${file["ProductPresentationId"]},
        ${concat(img64)}
      )
    ''';
    return await db.rawInsert(query);
  }

  destroyDatabase() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = directory.path + databaseName;
    await _database.close();
    await deleteDatabase(path);
    _database = null;
  }

  getScheduleEventsByDate(int employeeId, int planId, DateTime d) async {
    var db = await this.database;
    var query = '''
      select * from ${ScheduleEventTable.tableName}
      where ${ScheduleEventTable.planId}==$planId and
      ${ScheduleEventTable.employeeId}==$employeeId
    ''';
    var raws = await db.rawQuery(query);
    var result = raws.where((element) {
      return DateTime.parse(element[ScheduleEventTable.startTime]).day ==
              d.day &&
          DateTime.parse(element[ScheduleEventTable.startTime]).month ==
              d.month &&
          DateTime.parse(element[ScheduleEventTable.startTime]).year == d.year;
    }).toList();

    return result;
  }

  getScheduleEventsById(int eventId) async {
    var db = await this.database;
    var query = '''
      select * from ${ScheduleEventTable.tableName}
      where ${ScheduleEventTable.eventId}==$eventId
    ''';
    var result = await db.rawQuery(query);
    return result;
  }

  resetData() async {
    var db = await this.database;
    await db.rawDelete('delete from ${ActivePlanTable.tableName}');
    await db.rawDelete('delete from ${ScheduleEventTable.tableName}');
    await db.rawDelete('delete from ${PresentationsTable.tableName}');
    await db.rawDelete('delete from ${PresentationFilesTable.tableName}');
  }

  getActivePlans(int employeeId) async {
    var db = await this.database;
    List<dynamic> plansList = [];
    List plans = await db.rawQuery(
        'select * from ${ActivePlanTable.tableName} where ${ActivePlanTable.employeeId}==$employeeId');

    await Future.forEach(plans, (plan) async {
      dynamic tmpPlan = {
        ActivePlanTable.planId: plan[ActivePlanTable.planId],
        ActivePlanTable.planName: plan[ActivePlanTable.planName],
        ActivePlanTable.startDate: plan[ActivePlanTable.startDate],
        ActivePlanTable.endDate: plan[ActivePlanTable.endDate],
        ActivePlanTable.createdBy: plan[ActivePlanTable.createdBy],
        ActivePlanTable.employeeId: plan[ActivePlanTable.employeeId],
        "TotalVisits": await getTotalVisits(plan[ActivePlanTable.planId]),
        "AchievedVisits": await getAchievedVisits(plan[ActivePlanTable.planId]),
        "Categories":
            await getTotalCategoryCountByPlanId(plan[ActivePlanTable.planId]),
        "PendingSync": await getPendingSync(plan[ActivePlanTable.planId]),
      };
      plansList.add(tmpPlan);
    });
    return plansList;
  }

  getTotalVisits(int planId) async {
    var db = await this.database;
    var query =
        'select count(*) from ${ScheduleEventTable.tableName} where ${ScheduleEventTable.planId}==$planId';
    var result = await db.rawQuery(query);
    return result[0]["count(*)"];
  }

  getAchievedVisits(int planId) async {
    var db = await this.database;
    var query =
        'select count(*) from ${ScheduleEventTable.tableName} where ${ScheduleEventTable.planId}==$planId and ${ScheduleEventTable.isCall}==1';
    var result = await db.rawQuery(query);
    return result[0]["count(*)"];
  }

  getPendingSync(int planId) async {
    var db = await this.database;
    var query =
        'select count(*) from ${ScheduleEventTable.tableName} where ${ScheduleEventTable.planId}==$planId and ${ScheduleEventTable.needSync}==1';
    var result = await db.rawQuery(query);
    return result[0]["count(*)"];
  }

  getTotalCategoryCountByPlanId(int planId) async {
    var db = await this.database;
    List<dynamic> cat = [];
    var query = '''
    select
    ${ScheduleEventTable.categoryId} ,
    ${ScheduleEventTable.categoryName},
    ${ScheduleEventTable.isCall}
    from ${ScheduleEventTable.tableName} where ${ScheduleEventTable.planId}==$planId
    ''';
    List totalEvents = await db.rawQuery(query);
    if (totalEvents == null) return null;
    for (int i = 0; i < totalEvents.length; i++) {
      //print('before');
      var found = cat.firstWhere(
          (element) =>
              element["CategoryId"] ==
              totalEvents[i][ScheduleEventTable.categoryId],
          orElse: () => {});

      if (found[ScheduleEventTable.categoryId] != null) {
        //print(found);
        //print(found["Total"]);
        found["Total"] = found["Total"] + 1;

        if (totalEvents[i][ScheduleEventTable.isCall] == 1)
          found["Finished"] = found["Finished"] + 1;
      } else {
        dynamic temp = {
          "CategoryId": totalEvents[i][ScheduleEventTable.categoryId],
          "CategoryName": totalEvents[i][ScheduleEventTable.categoryName],
          "Total": 1,
          "Finished": (totalEvents[i][ScheduleEventTable.isCall] == 1 ? 1 : 0)
        };
        cat.add(temp);
      }
    }
    return cat;
  }

  startCall(int eventId) async {
    var db = await this.database;
    var query = '''
      update ${ScheduleEventTable.tableName} set ${ScheduleEventTable.callStartTime}='${DateTime.now().toString()}'
      where ${ScheduleEventTable.eventId}=$eventId
    ''';
    return await db.rawUpdate(query);
  }

  terminateCall(int eventId) async {
    var db = await this.database;
    var query = '''
      update ${ScheduleEventTable.tableName} set ${ScheduleEventTable.callEndTime}='${DateTime.now().toString()}' , ${ScheduleEventTable.isCall}=1
      where ${ScheduleEventTable.eventId}=$eventId
    ''';
    return await db.rawUpdate(query);
  }

  getPresentationsByPlanId(int planId) async {
    var db = await this.database;
    List<dynamic> presentationsList = [];

    List presentations = await db.rawQuery(
        'select * from ${PresentationsTable.tableName} where ${PresentationsTable.planId}==$planId');
    await Future.forEach(presentations, (present) async {
      dynamic tmppresent = {
        PresentationsTable.id: present[PresentationsTable.id],
        PresentationsTable.presentationName:
            present[PresentationsTable.presentationName],
        PresentationsTable.presentationDescription:
            present[PresentationsTable.presentationDescription],
        PresentationsTable.productId: present[PresentationsTable.productId],
        PresentationsTable.productLineId:
            present[PresentationsTable.productLineId],
        PresentationsTable.planId: present[PresentationsTable.planId],
        "Files": await getPresentationFilesByPresentationId(
            present[PresentationsTable.id])
      };
      presentationsList.add(tmppresent);
    });
    return presentationsList;
  }

  getPresentationFilesByPresentationId(int presentationId) async {
    var db = await this.database;
    List<dynamic> filesList = [];

    List files = await db.rawQuery(
        'select * from ${PresentationFilesTable.tableName} where ${PresentationFilesTable.productPresentationId}==$presentationId');

    await Future.forEach(files, (file) async {
      dynamic tmpfile = {
        PresentationFilesTable.id: file[PresentationFilesTable.id],
        PresentationFilesTable.fileName: file[PresentationFilesTable.fileName],
        PresentationFilesTable.productPresentationId:
            file[PresentationFilesTable.productPresentationId],
        PresentationFilesTable.img64: file[PresentationFilesTable.img64],
      };
      filesList.add(tmpfile);
    });
    return filesList;
  }

  concat(String val) {
    if (val == null) return null;
    return "'$val'";
  }
}
