
import 'package:maple/models/plan.dart';
import 'package:maple/providers/tables.dart';

class Factory {
  Plan toPlan(dynamic plan) {
    return Plan(
      id: plan[ActivePlanTable.planId],
      planName: plan[ActivePlanTable.planName],
      startDate: DateTime.parse(plan[ActivePlanTable.startDate]),
      endDate: DateTime.parse(plan[ActivePlanTable.endDate]),
      createdBy: plan[ActivePlanTable.createdBy],
      targetVisits: plan["TotalVisits"],
      achievedVisits: plan["AchievedVisits"],
      pendingSync: plan["PendingSync"],
      categories: (plan["Categories"] as List).map((cat) {
        return toCategory(cat);
      }).toList(),
    );
  }

  CategoryCount toCategory(dynamic cat) {
    return CategoryCount(
      categoryId: cat["CategoryId"],
      categoryName: cat["CategoryName"],
      targetCount: cat["Total"],
      achievedCount: cat["Finished"],
    );
  }

  ScheduleEvent toScheduleEvent(dynamic sce) {
    return ScheduleEvent(
      eventId: sce[ScheduleEventTable.eventId],
      planId: sce[ScheduleEventTable.planId],
      address: sce[ScheduleEventTable.address],
      doctorAddressId: sce[ScheduleEventTable.doctorAddressId],
      doctorEmail: sce[ScheduleEventTable.doctorEmail],
      doctorId: sce[ScheduleEventTable.doctorId],
      doctorMobile: sce[ScheduleEventTable.doctorMobile],
      doctorName: sce[ScheduleEventTable.doctorName],
      brickId: sce[ScheduleEventTable.brickId],
      brickName: sce[ScheduleEventTable.brickName],
      callStartTime: sce[ScheduleEventTable.callStartTime],
      callEndTime: sce[ScheduleEventTable.callEndTime],
      startTime: sce[ScheduleEventTable.startTime],
      endTime: sce[ScheduleEventTable.endTime],
      categoryId: sce[ScheduleEventTable.categoryId],
      categoryName: sce[ScheduleEventTable.categoryName],
      employeeId: sce[ScheduleEventTable.employeeId],
      isCall: sce[ScheduleEventTable.isCall],
      needSync: sce[ScheduleEventTable.needSync],
      specialityId: sce[ScheduleEventTable.specialityId],
      specialityName: sce[ScheduleEventTable.specialityName],
    );
  }

  Presentation toPresentation(dynamic presentation) {
    return Presentation(
      id: presentation[PresentationsTable.id],
      presentationName: presentation[PresentationsTable.presentationName],
      presentationDescription:
          presentation[PresentationsTable.presentationDescription],
      productId: presentation[PresentationsTable.productId],
      productLineId: presentation[PresentationsTable.productLineId],
      planId: presentation[PresentationsTable.planId],
      files: (presentation["Files"] != null
          ? (presentation["Files"] as List).map((f) => toPresentationFile(f)).toList()
          : null),
    );
  }

  PresentationFile toPresentationFile(dynamic file) {
    return PresentationFile(
      id: file[PresentationFilesTable.id],
      fileName: file[PresentationFilesTable.fileName],
      productPresentationId: file[PresentationFilesTable.productPresentationId],
      img64: file[PresentationFilesTable.img64]
    );
  }
}
