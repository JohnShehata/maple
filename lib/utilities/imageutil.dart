import 'dart:convert';
import 'dart:io' as Io;
import 'dart:typed_data';
import 'package:http/http.dart' as http;

import 'package:http/http.dart';

class ImageUtil {
  Future<String> downloadImage(String imageUrl) async {
    try
    {
      Response response = await http.get(imageUrl);
      final bytes = response?.bodyBytes;
      return (bytes != null ? base64Encode(bytes) : null);
    }
    catch(e)
    {
      return null;
    }

  }

  Uint8List loadImage(String imageText) {
    try
    {
      Uint8List decodedBytes = base64Decode(imageText);
      return decodedBytes;
    }
    catch(e)
    {
      return null;
    }

  }
  //base64ToImage(String baseImage) {}
}
