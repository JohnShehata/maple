import 'package:flutter/material.dart';

class CircleImage extends StatelessWidget {
  final double circleRadius;
  final String imagePath;
  CircleImage({
    this.circleRadius,
    this.imagePath,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: this.circleRadius,
      height: this.circleRadius,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        image: DecorationImage(
          image: AssetImage(this.imagePath),
        ),
      ),
    );
  }
}