import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:maple/constants.dart';
import 'package:maple/models/plan.dart';
import 'package:maple/widgets/headertext.dart';
import 'package:maple/widgets/progressbar.dart';

class PlanCard extends StatelessWidget {
  final Plan plan;
  PlanCard(this.plan);

  @override
  Widget build(BuildContext context) {
  print('plan.targetVisits');
  print(plan.targetVisits);
    return Card(
      elevation: 4,
      child: Container(
        color: kPrimaryLightColor,
        child: Padding(
          padding: const EdgeInsets.all(12),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  HeaderText(
                    headerName: 'Pending Sync',
                    headerColor: Colors.orange,
                    textColor: Colors.orange,
                    text: plan.pendingSync.toString(),
                  ),
                  HeaderText(
                    headerName: 'Achieved',
                    headerColor: Colors.green,
                    textColor: Colors.green,
                    text: plan.achievedVisits.toString(),
                  ),
                  HeaderText(
                    headerName: 'Total Visits',
                    headerColor: Colors.green,
                    textColor: Colors.green,
                    text: plan.targetVisits.toString(),
                  ),
                ],
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        HeaderText(
                          headerName: 'Plan Name',
                          text: plan.planName,
                        ),
                        SizedBox(height: 12),
                        HeaderText(
                          headerName: 'Start Date',
                          text: DateFormat.yMMMd()
                              .format(plan.startDate)
                              .toString(),
                        ),
                        SizedBox(height: 12),
                        HeaderText(
                          headerName: 'End Date',
                          text: DateFormat.yMMMd()
                              .format(plan.endDate)
                              .toString(),
                        ),
                        SizedBox(height: 12),
                        HeaderText(
                          headerName: 'Created By',
                          text: plan.createdBy,
                        ),
                      ],
                    ),
                    flex: 1,
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        SizedBox(height: 7),
                        ...plan.categories.map((cat) {
                          return Column(
                            children: [
                              ProgressBar(
                                title: cat.categoryName.toUpperCase(),
                                percent:
                                   ((cat.achievedCount / cat.targetCount) * 100).round().toDouble(),
                              ),
                              SizedBox(height: 12),
                            ],
                          );
                        }).toList()
                      ],
                    ),
                    flex: 1,
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
