import 'package:flutter/material.dart';
import 'package:maple/models/enums.dart';

class CallStatusCount extends StatelessWidget {
  final VisitStatusEnum status;
  final int count;

  CallStatusCount({this.status, this.count});

  Widget getStatusIcon() {
    if (this.status == VisitStatusEnum.NotVisited)
      return Icon(
        Icons.supervisor_account_outlined,
        color: Colors.orange,
      );
    else if (this.status == VisitStatusEnum.Visited)
      return Icon(
        Icons.supervisor_account,
        color: Colors.green,
      );
    else if (this.status == VisitStatusEnum.InProgress)
      return Icon(
        Icons.online_prediction,
        color: Colors.red,
      );
    return null;
  }

  Color getIconColor()
  {
    if (this.status == VisitStatusEnum.NotVisited)
      return Colors.orange;
    else if (this.status == VisitStatusEnum.Visited)
      return Colors.green;
    else if (this.status == VisitStatusEnum.InProgress)
      return Colors.red;
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        this.getStatusIcon(),
        SizedBox(
          width: 5,
        ),
        Text(
          this.count.toString(),
          style: TextStyle(color: this.getIconColor()),
        )
      ],
    );
  }
}
