import 'package:flutter/material.dart';
import 'package:maple/models/VisitNote.dart';

import '../constants.dart';

class VisitNoteCard extends StatelessWidget {
  final VisitNote note;

  VisitNoteCard(this.note);
  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(5),
      color: kPrimaryLightColor,
      elevation: 10,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  this.note.NoteTime,
                  style: TextStyle(color: Colors.white),
                ),
                Icon(Icons.event_note,color: Colors.white,)
              ],
            ),
            SizedBox(
              height: 7,
            ),
            Text(
              this.note.Note,
              maxLines: 10,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(color: Colors.white),
            )
          ],
        ),
      ),
    );
  }
}
