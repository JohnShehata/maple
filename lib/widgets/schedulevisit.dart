import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:maple/constants.dart';
import 'package:maple/models/enums.dart';
import 'package:maple/models/plan.dart';
import 'package:maple/screens/visitdetails.dart';
import 'headertext.dart';

class ScheduleVisit extends StatelessWidget {
  // final Visit visitDetails;
  // ScheduleVisit(this.visitDetails);
  final ScheduleEvent scheduleEvent;
  final CardVisitTypeEnum cardType;
  ScheduleVisit(this.scheduleEvent, this.cardType);

  Widget getCardIcon() {
    if (this.cardType == CardVisitTypeEnum.Details) return Text('');
    if (scheduleEvent.isCall == 1)
      return Icon(
        Icons.supervisor_account,
        color: Colors.green,
      );
    else if (scheduleEvent.callStartTime != null &&
        scheduleEvent.callEndTime == null)
      return Icon(
        Icons.online_prediction,
        color: Colors.red,
      );
    else
      return Icon(
        Icons.supervisor_account_outlined,
        color: Colors.orange,
      );
  }

  Color getIconColor() {
    if (scheduleEvent.isCall == 1)
      return Colors.green;
    else if (scheduleEvent.callStartTime != null &&
        scheduleEvent.callEndTime == null)
      return Colors.red;
    else
      return Colors.orange;
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Card(
      elevation: (this.cardType == CardVisitTypeEnum.Details ? 0 : 5),
      color: (this.cardType == CardVisitTypeEnum.Details
          ? kPrimaryColor
          : kPrimaryLightColor),
      child: Container(
        padding: EdgeInsets.all(15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  flex: 5,
                  child: HeaderText(
                    headerName: 'Doctor Name',
                    text: this.scheduleEvent.doctorName,
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: InkWell(
                    child: this.getCardIcon(),
                    onTap: () {
                      Navigator.of(context).pushNamed(VisitDetails.routeName,
                          arguments: {
                            'eventId': scheduleEvent.eventId,
                            'planId': scheduleEvent.planId
                          });
                    },
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 5,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: HeaderText(
                    headerName: 'Location',
                    text: this.scheduleEvent.address,
                  ),
                ),
                SizedBox(width: 10),
                Expanded(
                  child: HeaderText(
                    headerName: 'Brick Name',
                    text: this.scheduleEvent.brickName,
                  ),
                )
              ],
            ),
            SizedBox(
              height: 5,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: HeaderText(
                    headerName: 'Phone',
                    text: this.scheduleEvent.doctorMobile,
                  ),
                ),
                SizedBox(width: 10),
                Expanded(
                  child: HeaderText(
                    headerName: 'Email',
                    text: this.scheduleEvent.doctorEmail,
                  ),
                )
              ],
            ),
            SizedBox(
              height: 5,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: HeaderText(
                    headerName: 'Speciality',
                    text: this.scheduleEvent.specialityName,
                  ),
                ),
                SizedBox(width: 10),
                Expanded(
                  child: HeaderText(
                    headerName: 'Category',
                    text: this.scheduleEvent.categoryName?.toUpperCase(),
                  ),
                )
              ],
            ),
            SizedBox(
              height: 5,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.update_outlined,
                  color: this.getIconColor(),
                ),
                SizedBox(
                  width: 5,
                ),
                Text(
                  DateFormat('MMM dd, yyyy hh:mm a')
                      .format(DateTime.parse(this.scheduleEvent.startTime)),
                  style: TextStyle(color: this.getIconColor()),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
