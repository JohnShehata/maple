import 'package:flutter/material.dart';

class RoundedButton extends StatelessWidget {
  final String buttonLbl;
  final Function onPress;

  RoundedButton({this.buttonLbl,this.onPress});
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.8,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(29),
        child: FlatButton(
          onPressed: this.onPress,
          padding: EdgeInsets.symmetric(
            vertical: 20,
            horizontal: 40,
          ),
          child: Text(
            'LOGIN',
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          color: Colors.green[700],
        ),
      ),
    );
  }
}
