import 'package:flutter/material.dart';

import '../constants.dart';

class WaitingCirlce extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Center(
      child: Container(
        color: kPrimaryColor,
        height: size.width * 0.5,
        width: size.width * 0.5,
        margin: EdgeInsets.all(5),
        child: CircularProgressIndicator(
          strokeWidth: 7.0,
          valueColor: AlwaysStoppedAnimation(Colors.green),
        ),
      ),
    );
  }
}
