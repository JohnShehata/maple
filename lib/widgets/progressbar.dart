import 'package:flutter/material.dart';

class ProgressBar extends StatelessWidget {
  final String title;
  final double percent;

  ProgressBar({this.title,this.percent});

  Color barColor()
  {
    double p=this.percent;
    if(p>=0 && p<=50)
      return Colors.red;
    else if(p>=51 && p<=75)
      return Colors.orange;
    else
      return Colors.green;
  }



  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            this.title,
            style: TextStyle(
              color: Colors.grey,
              fontSize: 10,
            ),
          ),
          SizedBox(width: 5),
          Text('${this.percent}%',style: TextStyle(color: this.barColor()),)
        ],
      ),
      SizedBox(height: 5),
      Container(
        width: double.infinity,
        height: 3,
        decoration: BoxDecoration(
            color: Colors.grey[850],
            borderRadius: BorderRadius.circular(10)),
        child: Container(
          alignment: Alignment.centerLeft,
          child: FractionallySizedBox(
            widthFactor: this.percent/100,
            child: Container(
              width: double.infinity,
              height: 3,
              decoration: BoxDecoration(
                  color: this.barColor(),
                  borderRadius:
                  BorderRadius.circular(10)),
            ),
          ),
        ),
      ),
    ],);
  }
}
