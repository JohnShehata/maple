import 'package:flutter/material.dart';
import 'package:maple/screens/login.dart';
import 'package:maple/utilities/auth.dart';
import 'package:maple/widgets/profilepicture.dart';
import '../constants.dart';

class MyDrawer extends StatelessWidget {

  final auth=Auth();

  onPressYes() {}
  confirmDialog(BuildContext ctx) {
    showDialog(
      context: ctx,
      builder: (_) => AlertDialog(
        title: Text('Sign out Confirmation'),
        backgroundColor: kPrimaryLightColor,
        content: Text('Are you sure you want to sign out ?'),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.of(ctx).pop();
            },
            child: Text('Yes'),
          ),
          TextButton(
            onPressed: () {
              Navigator.of(ctx).pop();
            },
            child: Text('No'),
          )
        ],
      ),
      barrierDismissible: true,
    );
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Drawer(
      child: Container(
        height: double.infinity,
        width: double.infinity,
        color: kPrimaryColor,
        child: Column(
          children: [
            Container(
              height: size.height * 0.3,
              // color: Colors.red,
              width: double.infinity,
              padding: EdgeInsets.all(20),
              alignment: Alignment.centerLeft,
              child: ProfilePicture(
                imagePath: 'assets/images/john.PNG',
                circleRadius: size.width * 0.22,
                userCode: '#654',
                userName: 'John Shehata',
              ),
            ),
            Divider(
              height: 5,
              color: kPrimaryLightColor,
            ),
            ListTile(
              leading: Icon(
                Icons.work,
                color: Colors.white,
              ),
              title: Text(
                'Active plans',
                style: TextStyle(color: Colors.white),
              ),
            ),
            ListTile(
              leading: Icon(
                Icons.logout,
                color: Colors.white,
              ),
              title: InkWell(
                onTap: () {
                  showDialog(
                    context: context,
                    builder: (_) => AlertDialog(
                      title: Text('Sign out Confirmation'),
                      backgroundColor: kPrimaryLightColor,
                      content: Text('Are you sure you want to sign out ?'),
                      actions: [
                        TextButton(
                          onPressed: () async {
                            Navigator.of(context).pop();
                            await auth.logout();
                            Navigator.of(context).pushReplacementNamed(LoginScreen.routeName);
                          },
                          child: Text('Yes'),
                        ),
                        TextButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: Text('No'),
                        )
                      ],
                    ),
                    barrierDismissible: true,
                  );
                },
                child: Text(
                  'Sign out',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
