import 'package:flutter/material.dart';

import '../constants.dart';

class PasswordTextField extends StatefulWidget {
  final Function onSave;

  PasswordTextField({this.onSave});
  @override
  _PasswordTextField createState() => _PasswordTextField();
}

class _PasswordTextField extends State<PasswordTextField> {
 bool iconon = false;

  toggleIcon() {
    setState(() {
      this.iconon = !this.iconon;
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size=MediaQuery.of(context).size;

    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      padding: EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 5,
      ),
      width: size.width * 0.8,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(40),
        border: Border.all(color: kPrimaryColor),
      ),
      child: TextFormField(
        validator: (String value) {
          if (value.isEmpty) return 'Password is required';
          return null;
        },
        obscureText: !this.iconon,
        onSaved: widget.onSave,
        decoration: InputDecoration(
          icon: Icon(
            Icons.lock,
            color: kPrimaryColor,
          ),
          suffixIcon: InkWell(
            onTap: this.toggleIcon,
            child: Icon(
              (this.iconon ? Icons.visibility : Icons.visibility_off),
              color: kPrimaryColor,
            ),
          ),
          hintText: "Password",
          border: InputBorder.none,
        ),
      ),
    );
  }
}
