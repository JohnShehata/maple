import 'package:flutter/material.dart';

import '../constants.dart';

class EmailTextField extends StatefulWidget {
  final Function onSave;
  EmailTextField({this.onSave});

  @override
  _EmailTextFieldState createState() => _EmailTextFieldState();
}

class _EmailTextFieldState extends State<EmailTextField> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      padding: EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 5,
      ),
      width: size.width * 0.8,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(40),
        border: Border.all(color: kPrimaryColor),
      ),
      child: TextFormField(
        onSaved: widget.onSave,
        validator: (String value) {
          if (value.isEmpty) return 'Email is required';
          if (!RegExp(
                  r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
              .hasMatch(value)) return 'Invalid email format';
          return null;
        },
        decoration: InputDecoration(
          icon: Icon(
            Icons.person,
            color: kPrimaryColor,
          ),
          hintText: "Email",
          border: InputBorder.none,
        ),
      ),
    );
  }
}
