import 'package:flutter/material.dart';

class HeaderText extends StatelessWidget {
  final String headerName;
  final Color headerColor;
  final String text;
  final Color textColor;

  HeaderText({
    this.headerName,
    this.text,
    this.headerColor = Colors.grey,
    this.textColor = Colors.white,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Text(
          this.headerName,
          style: TextStyle(
            color: this.headerColor,
            fontSize: 10,
          ),
        ),
        SizedBox(height: 5),
        Text(
          this.text??'',
          style: TextStyle(
            color: this.textColor,
            fontSize: 14,
          ),
          textAlign: TextAlign.justify,
          maxLines: 5,
          overflow: TextOverflow.ellipsis,
        ),
        SizedBox(height: 5),
      ],
    );
  }
}
