import 'package:carousel_slider/carousel_options.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:maple/models/plan.dart';
import 'package:maple/screens/wideshow.dart';
import 'package:maple/utilities/imageutil.dart';

class SlideShow extends StatelessWidget {
  final Presentation presentation;
  final imgUtil = ImageUtil();
  //final String slideName;

  SlideShow({this.presentation});

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    Orientation orientation = MediaQuery.of(context).orientation;

    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              Icons.slideshow,
              color: Colors.white,
            ),
            SizedBox(
              width: 5,
            ),
            Text(
              this.presentation.presentationName,
              style: TextStyle(color: Colors.white, fontSize: 20),
            )
          ],
        ),
        SizedBox(height: 10),
        CarouselSlider(
          items: this.presentation.files.map((e) {
            return InkWell(
              onTap: () {
                Navigator.of(context).pushNamed(WideShow.routeName, arguments: {
                  'presentationId': this.presentation.id,
                  'fileId': e.id
                });
              },
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                    image: MemoryImage(imgUtil.loadImage(e.img64)),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            );
          }).toList(),
          options: CarouselOptions(
            enableInfiniteScroll: true,
            viewportFraction: 0.8,
            aspectRatio: 16 / 9,
            enlargeCenterPage: true,
            autoPlay: false,
            height: size.height *
                (orientation == Orientation.portrait ? 0.25 : 0.8),
          ),
        ),
      ],
    );
  }
}
