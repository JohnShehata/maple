import 'package:flutter/material.dart';

class DragLines extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size=MediaQuery.of(context).size;
    return Column(
      children: [
        Container(
          width: size.width * 0.1,
          height: 2,
          decoration: BoxDecoration(color: Colors.grey),
        ),
        SizedBox(
          height: 3,
        ),
        Container(
          width: size.width * 0.1,
          height: 2,
          decoration: BoxDecoration(color: Colors.grey),
        ),
      ],
    );
  }
}
