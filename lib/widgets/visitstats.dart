import 'package:flutter/material.dart';
import 'package:maple/models/enums.dart';

import '../constants.dart';
import 'callstatuscount.dart';

class VisitState extends StatelessWidget {
  @override
  final int doneCalls;
  final int inprogressCalls;
  final int unfinishedCalls;

  VisitState({
    this.doneCalls,
    this.inprogressCalls,
    this.unfinishedCalls,
  });

  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: kPrimaryColor,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(10),
          topRight: Radius.circular(10),
          bottomLeft: Radius.circular(10),
          bottomRight: Radius.circular(10),
        ),
      ),
      margin: EdgeInsets.symmetric(horizontal: 10),
      padding: EdgeInsets.all(10),
      child: Row(
        children: [
          CallStatusCount(
            count: this.doneCalls,
            status: VisitStatusEnum.Visited,
          ),
          CallStatusCount(
            count: this.unfinishedCalls,
            status: VisitStatusEnum.NotVisited,
          ),
          CallStatusCount(
            count: this.inprogressCalls,
            status: VisitStatusEnum.InProgress,
          ),
        ],
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
      ),
    );
  }
}
