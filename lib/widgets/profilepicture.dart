import 'package:flutter/material.dart';
import 'package:maple/widgets/circleimage.dart';

class ProfilePicture extends StatelessWidget {
  final double circleRadius;
  final String imagePath;
  final String userName;
  final String userCode;
  ProfilePicture(
      {this.circleRadius, this.imagePath, this.userName, this.userCode});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        CircleImage(
          circleRadius: this.circleRadius,
          imagePath: this.imagePath,
        ),
        SizedBox(height: 10),
        Container(
          width: double.infinity,
          alignment: Alignment.center,
          child: Text(
            this.userCode,
            style: TextStyle(
                // fontWeight: FontWeight.w700,
                fontSize: 15,
                color: Colors.grey[600]),
          ),
        ),
        Container(
          width: double.infinity,
          alignment: Alignment.center,
          child: Text(
            this.userName,
            style: TextStyle(
                // fontWeight: FontWeight.w700,
                fontSize: 17,
                color: Colors.grey[600]),
          ),
        )
      ],
    );
  }
}
