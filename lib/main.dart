import 'package:flutter/material.dart';
import 'package:maple/constants.dart';
import 'package:maple/providers/plans.dart';
import 'package:maple/screens/activeplans.dart';
import 'package:maple/screens/landing.dart';
import 'package:maple/screens/login.dart';
import 'package:maple/screens/myschedule.dart';
import 'package:maple/screens/test.dart';
import 'package:maple/screens/visitdetails.dart';
import 'package:maple/screens/wideshow.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    // Timer(Duration(seconds: 3), () {
    //   print(DateTime.now().toString());
    // });
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(
          value: PlansProvider(),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(backgroundColor: kPrimaryColor),
        home: LandingScreen(),
        routes: {
          ActivePlans.routeName: (ctx) => ActivePlans(),
          MySchedule.routeName: (ctx) => MySchedule(),
          VisitDetails.routeName: (ctx) => VisitDetails(),
          WideShow.routeName: (ctx) => WideShow(),
          LoginScreen.routeName: (ctx) => LoginScreen(),
          TestLoading.routeName:(ctx)=> TestLoading()
        },
      ),
    );
  }
}
