class User {
  final String name;
  final int userId;
  final String email;

  User({
    this.name,
    this.email,
    this.userId,
  });
}
