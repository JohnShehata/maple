enum VisitStatusEnum {
  NotVisited,
  Visited,
  InProgress
}

enum CardVisitTypeEnum
{
  Visit,
  Details
}

