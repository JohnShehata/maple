class Plan {
  final int id;
  final String planName;
  final DateTime startDate;
  final DateTime endDate;
  final String createdBy;
  final int targetVisits;
  final int achievedVisits;
  final int pendingSync;
  final List<CategoryCount> categories;

  Plan(
      {this.id,
      this.planName,
      this.startDate,
      this.endDate,
      this.createdBy,
      this.targetVisits,
      this.achievedVisits,
      this.categories,
      this.pendingSync});
}

class CategoryCount {
  final int categoryId;
  final String categoryName;
  final int targetCount;
  final int achievedCount;

  CategoryCount(
      {this.categoryId,
      this.categoryName,
      this.targetCount,
      this.achievedCount});
}

class ScheduleEvent {
  final int eventId;
  final String startTime;
  final String endTime;
  String callStartTime;
  String callEndTime;
  final int doctorId;
  final String doctorName;
  final String doctorEmail;
  final String doctorMobile;
  final int specialityId;
  final String specialityName;
  final int categoryId;
  final String categoryName;
  final int doctorAddressId;
  final String address;
  final int brickId;
  final String brickName;
  final int planId;
  final int isCall;
  final int employeeId;
  final int needSync;

  ScheduleEvent({
    this.eventId,
    this.startTime,
    this.endTime,
    this.callStartTime,
    this.callEndTime,
    this.doctorId,
    this.doctorName,
    this.doctorMobile,
    this.doctorAddressId,
    this.doctorEmail,
    this.specialityId,
    this.specialityName,
    this.brickId,
    this.brickName,
    this.categoryId,
    this.categoryName,
    this.planId,
    this.address,
    this.employeeId,
    this.isCall,
    this.needSync,
  });
}

class Presentation {
  final int id;
  final String presentationName;
  final String presentationDescription;
  final int productId;
  final int productLineId;
  final int planId;
  final List<PresentationFile> files;

  Presentation(
      {this.id,
      this.planId,
      this.presentationDescription,
      this.presentationName,
      this.productId,
      this.productLineId,
      this.files});
}

class PresentationFile {
  final int id;
  final String fileName;
  final int productPresentationId;
  final String img64;

  PresentationFile({
    this.id,
    this.fileName,
    this.productPresentationId,
    this.img64
  });
}
