import 'package:maple/models/enums.dart';

class Visit {
  final String doctorName;
  final String location;
  final String brickName;
  final String phoneNumber;
  final String email;
  final String speciality;
  final String category;
  final VisitStatusEnum visitStatus;
  final CardVisitTypeEnum cardType;
  final String appointmentTime;

  Visit({
    this.doctorName,
    this.location,
    this.brickName,
    this.phoneNumber,
    this.email,
    this.speciality,
    this.category,
    this.visitStatus,
    this.cardType,
    this.appointmentTime
  });
}
